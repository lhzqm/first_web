from django.shortcuts import render

# Create your views here.
from myblog.api import TopmenuXuliehua, BannerData
from myblog.models import Topmenu, Banner
from rest_framework.decorators import api_view
from rest_framework.response import Response


def index(request):
    # print(request)
    topmenu = Topmenu.objects.all()
    context = {
        'topmenu': topmenu,

    }
    return render(request, "index.html", context)


@api_view(['GET'])
def indexData(request):
    # 首页的导航栏
    topmenu = Topmenu.objects.all()
    topmenuData = TopmenuXuliehua(topmenu, many=True)

    # 首页的Banner
    banner = Banner.objects.all()
    bannerData = BannerData(banner, many=True)
    return Response({'topmenu': topmenuData.data, 'banner': bannerData.data})
