from rest_framework import serializers

# 用户管理
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate

from myblog.models import Topmenu, Banner


class TopmenuXuliehua(serializers.ModelSerializer):
    class Meta:
        depth = 1
        model = Topmenu
        fields = '__all__'


class BannerData(serializers.ModelSerializer):
    class Meta:
        depth = 1
        model = Banner
        fields = '__all__'

