new Vue({
    el: "#index",
    data: {
        topmenu: [],
        banner: []
    },
    mounted() {
        this.getData()
        console.log(this)
    },
    methods: {
        getData: function () {
            var self = this
            reqwest({
                url: '/api/index',
                method: 'get',
                type: 'json',
                success: function (data) {
                    console.log(data)
                    self.topmenu = data.topmenu
                    self.banner = data.banner
                    // console.log(self.topmenu)
                }
            })
        },

    }

});